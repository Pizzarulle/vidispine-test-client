# Vidispine-test-client

Console application that uses API to genrate and save a png-image of the Mandelbrot set.

# Dependencies
* OkHttp (https://mvnrepository.com/artifact/com.squareup.okhttp3/okhttp)
* JDK (to run project in IDE)
* Java IDE (IntelliJ of any other modern Java IDE)

# Install
* Clone project
* Open project in IDE and build

# Use 
* This project requires a running instace of https://gitlab.com/Pizzarulle/vidispine-test-server to be able to make the http-requests.
* In ide run main method 


