package HttpRequest;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

/**
 * Creates, sends and handle response from a http-request
 */
public class HttpsRequest implements  Runnable{

    private volatile BufferedImage img;
    private volatile String url;

    public HttpsRequest(String url) {
        this.url = url;
    }

    /**
     * Makes a https call to the api to generate a subImage of the mandelbrot set.
     * The api returns a byt-array of the image and its then converted to a bufferImage
     * @param url - url to the server with the right parameter values
     * @return {@link BufferedImage}
     * @throws IOException
     */
    public BufferedImage makeRequest(String url) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(url)
                .method("GET", null)
                .build();
        Response response = client.newCall(request).execute();
        final byte[] rawBytes = Objects.requireNonNull(response.body()).bytes();

        InputStream in = new ByteArrayInputStream(rawBytes);
        BufferedImage bImageFromConvert = ImageIO.read(in);

        return bImageFromConvert;

    }

    @Override
    public void run() {
        try {
            img = makeRequest(getUrl());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public BufferedImage getImg() {
        return img;
    }

    public void setImg(BufferedImage img) {
        this.img = img;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
