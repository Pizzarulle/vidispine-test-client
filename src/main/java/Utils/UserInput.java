package Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Uses scanner to collect user inputs in the console
 */
public class UserInput {

    /**
     * Gather user inputs with a scanner in one line.
     * The first three values must be in this order: maxIterations, x, y. And must be separated by spaces.
     * Every value after is the name/location of the servers. Must include a minimum of one server.
     * Cannot start or end with spaces.
     *
     * Example input format: 1024 1000 1000 localhost:8080
     * Example input format: 1024 1000 1000 localhost:8080 localhost:8080
     * Example input format: 1024 1000 1000 localhost:8080 localhost:8080 localhost:8080
     * @throws IOException
     */
    public void start() throws IOException, InterruptedException {
        Scanner sc = new Scanner(System.in);
        String inputCommand;
        inputCommand = sc.nextLine();

        String[] instructions = inputCommand.split(" ");

        List<String> servers = new ArrayList<>(Arrays.asList(instructions).subList(3, instructions.length));

        int maxIterations = Integer.parseInt(instructions[0]);
        int x = Integer.parseInt(instructions[1]);
        int y = Integer.parseInt(instructions[2]);


        CreateImageFile createImageFile = new CreateImageFile();

        createImageFile.createImage(x, y, maxIterations, servers);

    }
}
