package Utils;

import HttpRequest.HttpsRequest;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Create and save a png file in the project root folder
 */
public class CreateImageFile {

    /**
     * Creates and divides a bufferImage depending on the x, y variables and numbers of servers.
     * Each server returns a different part of the mandelbrotSet and is then added to a complete bufferImage.
     * <p>
     * When all subImages has return the complete image is saved in this projects root folder with the name "mandelbrot.png".
     *
     * @param x             width of the complete image
     * @param y             height of the  complete
     * @param maxIterations max number of iteratons
     * @param servers       {@link List}<{@link String}>> list of servers
     * @throws IOException
     * @throws InterruptedException
     */
    public void createImage(int x, int y, int maxIterations, List<String> servers) throws IOException, InterruptedException {
        BufferedImage mandelbrotSet = new BufferedImage(x, y, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = mandelbrotSet.createGraphics();

        int nrOfServers = servers.size();
        int individualImageHeight = y / nrOfServers;

        int start = 0;
        int end = individualImageHeight;

        //Makes asynchronous http request to each server and adds the return value to the current bufferImage
        for (int i = 0; i < nrOfServers; i++) {

            HttpsRequest hr = new HttpsRequest("http://" + servers.get(i) + "/mandelbrot/" + maxIterations + "/" + x + "/" + y + "/" + start + "/" + end + "/" + individualImageHeight + "/" + i);

            Thread newThread = new Thread(hr);
            newThread.start();
            newThread.join();
            g.drawImage(hr.getImg(), 0, start, null);

            start = end;
            end += individualImageHeight;
        }

        ImageIO.write(mandelbrotSet, "png", new File("mandelbrot.png"));

    }
}
