package com.example.vidispinecodetestclient;

import Utils.UserInput;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class VidispineCodeTestClientApplication {

    public static void main(String[] args) throws IOException, InterruptedException {
        SpringApplication.run(VidispineCodeTestClientApplication.class, args);

        UserInput userInput = new UserInput();
        userInput.start();

    }

}
